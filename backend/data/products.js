const products = [
  {
    name: 'Basil Plant',
    image: '/images/basil.png',
    description:
      'The dried large-leaf varieties have a fragrant aroma faintly reminiscent of anise and a warm, sweet, aromatic, mildly pungent flavour.',
    brand: 'Plant4U',
    category: 'Culinary Herb',
    price: 500,
    countInStock: 10,
    rating: 4.5,
    numReviews: 12,
  },
  {
    
    name: 'Bonzai Plant',
    image: '/images/bonzai.png',
    description:
      'Bonsai are trees and plants grown in containers in such a way so that they look their most beautiful--even prettier than those growing in the wild. ',
    brand: 'GreenGuy',
    category: 'Decorative',
    price: 1500,
    countInStock: 7,
    rating: 4.0,
    numReviews: 8,
  },
  {
    
    name: 'Snake Plant',
    image: '/images/snakeplant.png',
    description:
      'snake plants can absorb cancer-causing pollutants, including CO2, benzene, formaldehyde, xylene, and toluene. With the ability to absorb and remove harmful toxins, snake plants can act as an effective defense against airborne allergies',
    brand: 'Plant4U',
    category: 'Decorative',
    price: 200,
    countInStock: 5,
    rating: 3,
    numReviews: 12,
  },
  {
    
    name: 'Mini Cactus',
    image: '/images/cactus.png',
    description:
      'These mini-cacti generally produce brightly colored flowers and take on interesting shapes.',
    brand: 'SunFarm',
    category: 'Decorative',
    price: 399.99,
    countInStock: 11,
    rating: 5,
    numReviews: 12,
  },
  {
    
    name: 'Coffee Plant',
    image: '/images/coffee.png',
    description:
      ' Coffee plants have branches that are covered in dark green, waxy leaves that grow in pairs.',
    brand: 'SunFarm',
    category: 'Decorative',
    price: 249.99,
    countInStock: 7,
    rating: 3.5,
    numReviews: 10,
  },
  {
    
    name: 'String of Pearls',
    image: '/images/pearl.png',
    description:
      ' Senecio rowleyanus, is a flowering plant in the daisy family Asteraceae. It is a creeping, perennial, succulent vine native to the drier parts of southwest Africa',
    brand: 'Plant4U',
    category: 'Electronics',
    price: 529.99,
    countInStock: 0,
    rating: 4,
    numReviews: 12,
  },
]

export default products;
