import bcrypt from 'bcryptjs';

const users = [
    {
        name:'Admin User',
        email:'admin@example.com',
        password: bcrypt.hashSync('123456',10),
        isAdmin: true,
    },
    {
        name:'John Dope',
        email:'jDope@example.com',
        password: bcrypt.hashSync('123456',10),
    },
    {
        name:'Jane Dope',
        email:'janeDope@example.com',
        password: bcrypt.hashSync('123456',10),
    },
]

export default users;