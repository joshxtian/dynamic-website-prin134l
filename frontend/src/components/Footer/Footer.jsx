import React from 'react'
import './Footer.css'

const Footer = () => {
    return (
        <footer>
            Copyright &copy; GreenBay
        </footer>
    )
}

export default Footer
