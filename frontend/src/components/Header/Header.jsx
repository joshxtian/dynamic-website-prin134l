import React, {useEffect} from 'react';
import './Header.css';
import {Link, Route} from 'react-router-dom';

import SearchBar from '../SearchBar/SearchBar';
import {LinkContainer} from 'react-router-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';
import { NavDropdown } from 'react-bootstrap';
import { logout } from '../../actions/userActions';
import {removeAllFromCart} from '../../actions/cartActions';

const Header = () => {
    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart);
    const userLogin = useSelector(state=>state.userLogin);
    const {userInfo} = userLogin;

    const {cartItems} = cart;
    
    const logoutHandler = ()=>{
        dispatch(logout())
        dispatch(removeAllFromCart());
    };


    return (
        <nav className='header'>
            
            <div className="header__top">
                <ul className="header__link-list">
                    {userInfo && userInfo.isAdmin && 
                        <NavDropdown title="Admin Panel" id="adminmenu">
                            <LinkContainer to='/admin/userlist'>
                                <NavDropdown.Item>Users</NavDropdown.Item>
                            </LinkContainer>
                            <LinkContainer to='/admin/productlist'>
                                <NavDropdown.Item>Products</NavDropdown.Item>
                            </LinkContainer>
                            <LinkContainer to='/admin/orderlist'>
                                <NavDropdown.Item>Orders</NavDropdown.Item>
                            </LinkContainer>
                        </NavDropdown>
                    }
                    {userInfo ? (
                        <NavDropdown title={userInfo.name}>
                            <LinkContainer to='/profile'>
                                <NavDropdown.Item>Profile</NavDropdown.Item>
                            </LinkContainer>
                            
                            <NavDropdown.Item onClick={logoutHandler}>
                                Logout
                            </NavDropdown.Item>
                        </NavDropdown>
                    ) : (
                        <li className="header__link-item">
                            <LinkContainer to="/login">
                                <Link>Login</Link>
                            </LinkContainer>
                        </li>
                    )}
                        
                </ul>
              
            </div>
            <div className="header__wrapper">
                <div className="header__hamburger">
                    <a>
                        <MenuIcon className="header__hamburgerIcon"/>
                    </a>
                </div>
                <Link to="/" className="header__logo-wrapper">
                    <img className="header__logo" src="/src/img/greenbay-logo.png"></img>
                </Link>
                
                <Route render={({history})=><SearchBar history={history}/>}/>
                <div className="header__search-md">
                    <SearchIcon className="header__find-logo"/>
                </div>
                <div className="header__basket"> 
                    <Link className="header__basketLink" to={userInfo ? "/cart" : "/login" }>
                        <div className="header__iconCount">
                            <i className="fas fa-shopping-basket"></i>

                            {cartItems.length > 0 && (
                                <div className="header__count">
                                    <span>{cartItems.reduce((acc,item)=>acc+item.qty, 0)}</span>
                                </div>
                            )}
                            
                        </div>
                    </Link>
                    
                </div>

            </div>   
    </nav>
    )
}

export default Header
