import React from 'react';
import {Helmet} from 'react-helmet';

const Meta = ({title,description,keyword}) => {
    return (
        <Helmet>
            <title>{title}</title>
            <meta name="description" content={description}/>
            <meta name="keyword" content={keyword}/>
        </Helmet>
    )
}

Meta.defaultProps = {
    title: "Welcome to GreenBay!",
    description:"We sell the greenest greens.",
    keyword: "The Freshest Green Exported Right out Of our Farm!"
}

export default Meta
