import React from 'react';
import './Notification.css';

const Notification = ({name, image}) => {
    return (
        <div className="notification">
            <div className="notification__image">
                <img src={image}></img>
            </div>
            <div className="notification__info">
                <p>
                    <span class="notification__title">{name.length < 45 ? name : name.substring(0,45) + "..." }</span>
                    <span class="notification__msg">Added to cart</span>
                </p>
            </div>
            
        </div>
    )
}

export default Notification
