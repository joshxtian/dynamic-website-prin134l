import React from 'react';
import './Product.css';
import {Link} from 'react-router-dom';
import {Card} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import {addToCart} from '../../actions/cartActions.js';
import { toast } from 'react-toastify';
import Notification from '../Notification/Notification';
import Rating from '../Rating/Rating';

const Product = ({product}) => {

    const dispatch = useDispatch();


    const addSingleToCartHandler = () =>{
        dispatch(addToCart(product._id,1));

        toast( 
          <Notification name={product.name} image={product.image}/>
        );
    }


    return (
        <Card className='my-3 rounded'>

            <Link to={`/product/${product._id}`}>
                <Card.Img src={product.image} variant='top'/>
            </Link>
            <Card.Body className='product__cardBody px-3 py-1'>
                <Link className="product__linkText" to={`/product/${product._id}`}>
                    <Card.Title>
                        <strong>{product.name}</strong>
                    </Card.Title>
                </Link>
                    
                <Rating value={product.rating}/>
                <Card.Text as='h3'>
                    ₱{product.price}
                </Card.Text>

                {product.countInStock === 0 ? (
                    <div className="product__addToCart product__OutofStock">
                        <span>Out Of Stock</span>
                    </div>
                ) : (
                <div className="product__addToCart" onClick={addSingleToCartHandler}>
                    <i class="fas fa-shopping-basket"></i>
                </div>
                )}
                
            </Card.Body>
        </Card>
    )
}

export default Product
