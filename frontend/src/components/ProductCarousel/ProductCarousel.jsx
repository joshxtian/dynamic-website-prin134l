import React, {useEffect} from 'react';
import './ProductCarousel.css'
import {Link} from 'react-router-dom';
import {Carousel, Image, } from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import Loader from '../Loader/Loader';
import Message from '../Message/Message';
import {listTopProducts} from '../../actions/productActions';

const ProductCarousel = () => {
    const dispatch = useDispatch();
    const productTopRated = useSelector(state=>state.productTopRated);
    const {loading, error, products} = productTopRated;
    useEffect(() => {
        dispatch(listTopProducts());
    }, [dispatch])
    return loading ? <Loader/> : error ? <Message variant="danger">{Error}</Message> : (
        <Carousel pause="hover" className="bg-dark">
            {products.map(product => {
                return <Carousel.Item>
                    <Link className="carousel__item-image" to={`/product/${product._id}`}>
                        <Image  src={product.image} alt={product.name} fluid/>
                    </Link>
                    <Carousel.Caption className="carousel-caption">
                        <h2 className="carousel__description">{product.name} (₱{product.price})</h2>
                    </Carousel.Caption>
                </Carousel.Item>
            })}
        </Carousel>
    )
}

export default ProductCarousel
