import React, {useState,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';

const SearchBar = ({history}) => {

    const [keyword, setKeyword] = useState('');

    useEffect(() => {
       console.log(keyword);
    }, [keyword])


    const submitHandler = (e) =>{
        e.preventDefault();
        if(keyword.trim()){
            history.push(`/search/${keyword}`);
        }
        else{
            history.push("/");
        }
    }

    return (
        <Form className="header__search-form" onSubmit={submitHandler} inline>
            <Form.Control type="text" name="q" placeHolder="Search Products..." value={keyword} onChange={e=>setKeyword(e.target.value)} className="mr-sm-2 ml-sm-5"></Form.Control>
            <Button type="submit" variant="outline-success" className="p-2">Search</Button>
        </Form>
    )
}

export default SearchBar
