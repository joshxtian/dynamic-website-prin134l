import React, {useState, useEffect} from 'react';
import './CartScreen.css';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {Card,Row , Col , ListGroup, Image, Form, Button , Cart} from 'react-bootstrap';
import {addToCart, removeFromCart} from '../../actions/cartActions.js';
import Message from '../../components/Message/Message';

const CartScreen = ({match, location, history}) => {
    const productId = match.params.id;
    const qty = location.search ? Number(location.search.split('=')[1]) : 1;

    const dispatch = useDispatch();

    const cart = useSelector(state => state.cart);
    const userLogin = useSelector(state=>state.userLogin);
    const {userInfo} = userLogin;
    const {cartItems}=cart;

    useEffect(() => {
        if(productId){
            dispatch(addToCart(productId, qty));
        }
        
    }, [dispatch, productId, qty]);

    const removeFromCartHandler = (id) =>{
        dispatch(removeFromCart(id));

    }
    const checkoutHandler = ()=>{
        if(!userInfo){
            history.push('/login');
        } else{
            history.push('/shipping');
        }

    }
    return (
        <div className="cartscreen py-4">
            
            <Row>
                <h1 className="section-header">Shopping Basket</h1>
                {cartItems.length === 0 ? (
                    <div className="cartscreen__empty">

                        <div className="cartscreen__text">
                            <h1>Your Shopping Cart Is Empty!</h1>
                            <Link to="/">
                                <div className="cartscreen__empty-back">
                                    Shop Now
                                </div>
                            </Link>
                            
                        </div>
                        
                        
                    </div>
                )
                :( 
                    <>
                    <Col md={8}>
                    
                   <ListGroup variant="flush">
                            {cartItems.map(item=>{
                                
                            return <ListGroup.Item className="my-1" key={item._id}>
                                    <Row>
                                        <Col md={2}>
                                            <Image src={item.image} alt={item.name} fluid rounded/>
                                        </Col>
                                        <Col md={2}>
                                            <Link to={`/product/${item._id}`}><h1 className="cartscreen__product-name">{item.name}</h1></Link>
                                        </Col>
                                        <Col md={2}>
                                            <h5>₱{item.price}</h5>
                                        </Col>
                                        <Col md={3}>
                                           
                                             <Form.Control as='select' value={item.qty} onChange={(e)=>{
                                                 dispatch(addToCart(item._id, Number(e.target.value)));
                                             }}>
                                             {[...Array(item.countInStock).keys()].map(x => (
                                                 <option key={x + 1} value={x+1}>
                                                     {x+1}
                                                 </option>
                                             ))}
                                             </Form.Control>
                                       
                                        </Col>
                                        <Col md={2}>
                                            <Button type="button" variant="light" onClick={()=>removeFromCartHandler(item._id)}>
                                                <i className="fas fa-trash"></i>
                                            </Button>
                                        </Col>
                                    </Row>
                                </ListGroup.Item>
                            })}
                        </ListGroup>
                    </Col>
                    <Col md={3}>
                        <Card>
                            <ListGroup>
                                <ListGroup.Item>
                                    <h2>Subtotal ({cartItems.reduce((acc,item)=>acc+item.qty, 0)}) </h2>
                                    ₱{cartItems.reduce((acc,item)=> acc+item.qty * item.price,0).toFixed(2)}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <Button type="button" className="btn btn-lg btn-block cartscreen__checkout" disabled={cartItems.length === 0} onClick={checkoutHandler}>Proceed To Checkout</Button>
                                </ListGroup.Item>
                            </ListGroup>
                        </Card>      
                    </Col>
                    </>
                )}

                
            </Row>
        </div>
        
    )
}

export default CartScreen
