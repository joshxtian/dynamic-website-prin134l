import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import Meta from '../../components/Meta/Meta';
import { useDispatch, useSelector} from 'react-redux';
import {Row, Col, Container, Button} from 'react-bootstrap';
import Loader from '../../components/Loader/Loader';
import Message from '../../components/Message/Message';
import ProductCarousel from '../../components/ProductCarousel/ProductCarousel';
import Paginate from '../../components/Paginate/Paginate';
import './HomeScreen.css';
import Product from '../../components/Product/Product';
import {listProducts} from '../../actions/productActions.js';

const HomeScreen = ({match}) => {

    const keyword = match.params.keyword;
    const dispatch = useDispatch();
    const pageNumber = match.params.pageNumber || 1;

    const productList = useSelector((state)=>state.productList);
    const { loading, error, products, page, pages} = productList;

    useEffect(() => {
        dispatch(listProducts(keyword,pageNumber))
    }, [dispatch,keyword,pageNumber])

    return (
        <>
            <Meta />
            
            <div className="homescreen py-4">
               
                <div className="homescreen__content">
                {!keyword ? <ProductCarousel/> : <Link to="/">
                    <Button className="btn btn-light">Go Back</Button>
                </Link>}
                    <h1 className="homescreen__header section-header">Latest Greens</h1>
                        {loading ? <Loader/> : error ? <Message variant="danger">{error}</Message>: (
                        <>
                            <Row>
                                {products.map(product=> {
                                    return (
                                        <Col sm={12}    md={6} lg={4}  xl={3}>
                                            <Product    product=   {product}/>
                                        </Col>)
                                })}
                            </Row>
                            <Paginate pages={pages} page={page} keyword={keyword ? keyword : ""}/>
                        </>
                    )}
                  
                   
                  
                   
            </div>
        </div>
        </>
       
    )
}

export default HomeScreen;
