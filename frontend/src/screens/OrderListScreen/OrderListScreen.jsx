import React,{useEffect} from 'react';
import './OrderListScreen.css';
import {LinkContainer} from 'react-router-bootstrap';
import {Row,Col,Button,Table,List,ListGroup} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import Message from '../../components/Message/Message';
import Loader from '../../components/Loader/Loader';
import {listOrders} from '../../actions/orderActions';
import {ORDER_LIST_RESET} from '../../constants/orderConstants';
const OrderListScreen = ({history, match}) => {
    
    const dispatch = useDispatch();

  
    const orderList = useSelector(state=>state.orderList);
    const {loading, error, orders} = orderList;
    
    const userLogin = useSelector(state=>state.userLogin);
    const {userInfo} = userLogin;

    
    useEffect(()=>{
        
        dispatch({type:ORDER_LIST_RESET});

        if(userInfo.isAdmin){
            dispatch(listOrders());
        
            
        }
        else{
            history.push("/login")
        }
    }, [dispatch, history,userInfo]);

    const deleteOrderHandler = (id) => {
        if(window.confirm("Are You Sure?")){
        //    dispatch(deleteProduct(id));
        }
        
    }
    const createProductHandler = (id) => {
        // dispatch(createProduct());
    }
    return (
        <>  
            <Row className="align-items-center">
                <Col>
                    <h1>Orders</h1>
                </Col>
               
            </Row>
            {/* {loadingDelete && <Loader/>} 
            {errorDelete && <Message variant="danger">{errorCreate}</Message>}
            {loadingCreate && <Loader/>} 
            {errorCreate && <Message variant="danger">{errorCreate}</Message>} */}
            {loading ? <Loader/> : error ? <Message variant="danger">{error}</Message> : (
                <Table striped bordered hover responsive className="table-sm">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Total</th>
                            <th>Paid</th>
                            <th>Delivered</th>
                            <th>Payment Method</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders.map(order=>{
                            return (<tr id={order._id}>
                                <td>{order._id}</td>
                                <td>{order.user.name}</td>
                                <td>{order.createdAt.substring(0,10)}</td>
                                <td>₱{order.totalPrice}</td>
                                <td>{order.isPaid ? (
                                    <>Paid at {order.paidAt.substring(0,10)} </>
                                ) : (
                                    <i class="fas fa-times" style={{color:"red"}}></i>
                                )}</td>
                                <td>{order.isDelivered ? (
                                    <>Delivered at {order.deliveredAt.substring(0,10)} </>
                                ) : (
                                    <i class="fas fa-times" style={{color:"red"}}></i>
                                )}</td>
                                <td>{order.paymentMethod}</td>
                                <td>
                                    <LinkContainer to={`/orders/${order._id}`}>
                                        <Button variant="light" className="btn-sm">
                                            DETAILS
                                        </Button>
                                    </LinkContainer>
                                    
                                </td>
                            </tr>)
                            })}
                    </tbody>
                </Table>
            )}
            
        </>
    )
}

export default OrderListScreen;
