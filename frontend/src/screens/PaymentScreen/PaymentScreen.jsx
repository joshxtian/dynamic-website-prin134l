import React, {useState} from 'react';
import './PaymentScreen.css';
import FormContainer from '../../components/FormContainer/FormContainer';
import { Form, Button, Col, Container} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import {savePaymentMethod} from '../../actions/cartActions';
import CheckoutSteps from '../../components/CheckoutSteps/CheckoutSteps';


const PaymentScreen = ({history}) => {
    const dispatch = useDispatch();
    const cart = useSelector(state=>state.cart);
    const {shippingAddress} = cart;

    if(!shippingAddress){
        history.push('/shipping');
    }


    const [paymentMethod, setPaymentMethod] = useState('PayPal');
    const [city, setCity] = useState(shippingAddress.city);
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode);
    const [country, setCountry] = useState(shippingAddress.country);

    const submitHandler = (e) =>{
        e.preventDefault();
        dispatch(savePaymentMethod(paymentMethod));

        history.push('/placeorder');
    }
    return (
        <FormContainer>
            <CheckoutSteps step1 step2 step3 step4/>
            <h1>Payment Method</h1>
            <Form onSubmit={submitHandler}>
                 <Form.Group>
                     <Form.Label as='legend'>Select Method</Form.Label>
                     <Col>
                         <Form.Check type='radio' label='PayPal or Credit Card' id='PayPal' name='paymentMethod' value='PayPal' checked onChange={e=>setPaymentMethod(e.target.value)}>
                             
                         </Form.Check>
                         {/* <Form.Check type='radio' label='Stripe' id='Stripe' name='paymentMethod' value='Stripe' onChange={e=>setPaymentMethod(e.target.value)}>
                             
                         </Form.Check> */}
                     </Col>

                     
                 </Form.Group>
                 <Button type="submit" variant="primary">Continue</Button>
            </Form>
        </FormContainer>
    )
}

export default PaymentScreen;
