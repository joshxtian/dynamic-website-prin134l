import React,{useEffect} from 'react';
import './ProductListScreen.css';
import {LinkContainer} from 'react-router-bootstrap';
import {Row,Col,Button,Table} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import Message from '../../components/Message/Message';
import Loader from '../../components/Loader/Loader';
import Paginate from '../../components/Paginate/Paginate';
import {listProducts, deleteProduct, createProduct} from '../../actions/productActions.js';
import {PRODUCT_CREATE_RESET} from '../../constants/productConstants';
const ProductListScreen = ({history, match}) => {
    const pageNumber = match.params.pageNumber || 1;
    const dispatch = useDispatch();

    const productList = useSelector(state=>state.productList);
    const {loading, error, products, page, pages} = productList;
    
    const productDelete = useSelector(state=>state.productDelete);
    const {loading:loadingDelete, error:errorDelete, success:successDelete} = productDelete;

    const productCreate = useSelector(state=>state.productCreate);
    const {loading:loadingCreate, error:errorCreate, success:successCreate, product: createdProduct} = productCreate;

    const userLogin = useSelector(state=>state.userLogin);
    const {userInfo} = userLogin;

    
    useEffect(()=>{
        
        dispatch({type:PRODUCT_CREATE_RESET});

        if(userInfo.isAdmin){
            dispatch(listProducts('', pageNumber));
            
        }
        else{
            history.push("/login")
        }
        if(successCreate){
            history.push(`/admin/product/${createdProduct._id}/edit`)
        }
        
    }, [dispatch,history,userInfo,successDelete,successCreate,createdProduct, pageNumber]);

    const deleteProductHandler = (id) => {
        if(window.confirm("Are You Sure?")){
           dispatch(deleteProduct(id));
        }
        
    }
    const createProductHandler = (id) => {
        dispatch(createProduct());
    }
    return (
        <>  
            <Row className="align-items-center">
                <Col>
                    <h1>Products</h1>
                </Col>
                <Col className="text-right">
                    <Button className="btn btn-success my-3 btn-add" onClick={createProductHandler}>
                        <i className="fas fa-plus"> Create Product</i>
                    </Button>
                </Col>
            </Row>
            {loadingDelete && <Loader/>} 
            {errorDelete && <Message variant="danger">{errorCreate}</Message>}
            {loadingCreate && <Loader/>} 
            {errorCreate && <Message variant="danger">{errorCreate}</Message>}
            {loading ? <Loader/> : error ? <Message variant="danger">{error}</Message> : 
                <>
                    <Table striped bordered hover responsive className="table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Brand</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map(product=>{
                                return (<tr id={product._id}>
                                    <td>{product._id}</td>
                                    <td>{product.name}</td>
                                    <td>₱{product.price}</td>
                                    <td>{product.category}</td>
                                    <td>{product.brand}</td>
                                    <td>
                                        <LinkContainer to={`/admin/product/${product._id}/  edit`}>
                                            <Button variant="light" className="btn-sm">
                                                <i className="fas fa-edit"></i>
                                            </Button>
                                        </LinkContainer>
                                        <Button variant="danger" className="btn-sm"     onClick={()=>{
                                            deleteProductHandler(product._id);
                                        }}>
                                            <i className="fas fa-trash-alt"></i>
                                        </Button>
                                    </td>
                                </tr>)
                                })}
                        </tbody>
                    </Table>
                    <Paginate pages={pages} page={page} isAdmin={true}></Paginate>
                </>
            }
            
        </>
    )
}

export default ProductListScreen
