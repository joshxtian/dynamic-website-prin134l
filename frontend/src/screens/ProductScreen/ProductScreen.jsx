import React, {useState ,useEffect} from 'react';
import './ProductScreen.css';
import { useDispatch, useSelector} from 'react-redux';
import Meta from '../../components/Meta/Meta';
import { Link } from 'react-router-dom';
import {Row,Col, ListGroup, Image, Card, Button, Form} from 'react-bootstrap';
import Rating from '../../components/Rating/Rating';
import {addToCart} from '../../actions/cartActions.js';
import Loader from '../../components/Loader/Loader';
import Message from '../../components/Message/Message';
import {listProductDetails, createProductReview} from '../../actions/productActions.js';
import {PRODUCT_CREATE_REVIEW_RESET} from '../../constants/productConstants';
const ProductScreen = ({history,match}) => {
    const [qty, setQty] = useState(1);
    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState("");

    const dispatch = useDispatch();

    const productDetails = useSelector(state=>state.productDetails);
    const {loading, product, error} = productDetails;

    const userLogin = useSelector(state=>state.userLogin);
    const {userInfo} = userLogin;
    
    const productReviewCreate = useSelector(state=>state.productReviewCreate);
    const {success:successProductReview,error:errorProductReview} = productReviewCreate;

    useEffect(()=>{
        if(successProductReview){
            alert("Review Submitted!");
            setRating(0);
            setComment("");
            dispatch({type:PRODUCT_CREATE_REVIEW_RESET});
        }
        dispatch(listProductDetails(match.params.id))
        
    },[dispatch, successProductReview ,match]);

    
    const addToCartHandler = ()=>{
        dispatch(addToCart(product._id, qty));
       history.push(`/cart/${match.params.id}?qty=${qty}`);
    }

    const submitHandler = (e) =>{
        e.preventDefault();
        dispatch(createProductReview(match.params.id,{rating,comment}))
    }

    return (
    <div className="py-4">
          <Meta title={product.name}/>
        <Link to="/" className="btn btn-light">
            Go Back
        </Link>

        {loading ? <Loader/> : error ? <Message variant="danger">{error}</Message>:(
            <div className="product my-2">
                        
            <Row>
                <Col lg={6} md={6}>
                    <Image fluid src={product.image}/>
                </Col>
                <Col lg={6} md={3}>
                    <ListGroup className="product__listGroup py-4">
                        <ListGroup.Item>
                            <h2 className="product__productName">{product.name}</h2>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Rating value={product.rating}/>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <h2 className="product__productPrice">₱{product.price}</h2>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Description: {product.description}
                        </ListGroup.Item>
                        <ListGroup.Item>
                        
                            {product.countInStock > 0 ? <h1 className="product__inStock">
                                In Stock
                            </h1> : <h1 className="product__outStock">
                                Out of Stock
                            </h1>}
                        </ListGroup.Item>
                        {product.countInStock > 0 && (
                                <ListGroup.Item>
                                    <Row>
                                        <Col md={1}>
                                            QTY:
                                        </Col>
                                        <Col md={11}>
                                            <Form.Control as='select' value={qty} onChange={(e)=>{
                                                setQty(e.target.value);
                                            }}>
                                                {[...Array(product.countInStock).keys()].map(x => (
                                                    <option key={x + 1} value={x+1}>
                                                        {x+1}
                                                    </option>
                                                ))}
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                </ListGroup.Item>
                            )}
                                                
                            <ListGroup.Item>
                                <Button className='btn-block btn-lg product__addToCartButton' onClick={addToCartHandler} type='button' disabled={product.countInStock === 0}>
                                    Add To Cart
                                </Button>
                            </ListGroup.Item>
                    </ListGroup>
                </Col>
                                                
            </Row>
           
            </div>        
            )}
            <Row>
                <Col md={6}>
                    <h2>Reviews</h2>
                    {product.reviews.length === 0 && <Message>No Reviews Yet!</Message>}
                    <ListGroup>
                          {product.reviews.map(item=>{
                              return <ListGroup.Item key={item._id}>
                                  <strong>{item.name}</strong>
                                  <Rating value={item.rating}/>
                                  <p>{item.createdAt.substring(0,10)}</p>
                                  <p>{item.comment}</p>
                              </ListGroup.Item>
                              
                          })}      
                        <ListGroup.Item>
                          <h2>Write A Customer Review</h2>
                          {errorProductReview && <Message varian="danger">{errorProductReview}</Message>}
                          {userInfo ? (
                              <Form onSubmit={submitHandler}>
                                  <Form.Group controlId="rating">
                                      <Form.Label>Rating</Form.Label>
                                      <Form.Control as="select" value={rating} onChange={e=>setRating(e.target.value)}>
                                          <option value=''>Select...</option>
                                          <option value='1'>1 - Poor</option>
                                          <option value='2'>2 - Fair</option>
                                          <option value='3'>3 - Good</option>
                                          <option value='4'>4 - Very Good</option>
                                          <option value='5'>5 - Excellent</option>
                                      </Form.Control>
                                  </Form.Group>
                                  <Form.Group controlId="comment">
                                      <Form.Label>Comment</Form.Label>
                                      <Form.Control as="textarea" row="3" value={comment} onChange={e=>setComment(e.target.value)} placeHolder="Type A Review"></Form.Control>
                                  </Form.Group>
                                  <Button type="submit" variant="primary">Submit</Button>
                              </Form>
                          ) : 
                          <Message> Please <Link to="/login">Sign In</Link>
                            to write a review
                          </Message>}
                        </ListGroup.Item>                    
                    </ListGroup>
                </Col>
            </Row>                                        
        </div>
        
    )
}

export default ProductScreen
