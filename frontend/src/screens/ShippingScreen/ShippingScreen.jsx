import React, {useState} from 'react';
import './ShippingScreen.css';
import FormContainer from '../../components/FormContainer/FormContainer';
import { Form, Button} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import {saveShippingAddress} from '../../actions/cartActions';
import CheckoutSteps from '../../components/CheckoutSteps/CheckoutSteps';

const ShippingScreen = ({history}) => {
    const dispatch = useDispatch();
    const cart = useSelector(state=>state.cart);
    const {shippingAddress} = cart;
    const [address, setAddress] = useState(shippingAddress.address);
    const [city, setCity] = useState(shippingAddress.city);
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode);
    const [country, setCountry] = useState(shippingAddress.country);

    const submitHandler = (e) =>{
        e.preventDefault();
        dispatch(saveShippingAddress({address,city,postalCode,country,}));

        history.push('/payment');
    }
    return (
        <FormContainer>
            <CheckoutSteps step1 step2 step3 step4/>
            <h1>Shipping</h1>
            <Form onSubmit={submitHandler}>
                 <Form.Group className="my-3" controlId="address">
                    <Form.Label>Address</Form.Label>
                    <Form.Control type="text" placeholder="Enter Address" required value={address} onChange={e => setAddress(e.target.value)}></Form.Control>
                </Form.Group>

                <Form.Group className="my-3" controlId="city">
                    <Form.Label>City</Form.Label>
                    <Form.Control type="text" placeholder="Enter City" required value={city} onChange={e => setCity(e.target.value)}></Form.Control>
                </Form.Group>

                
                <Form.Group className="my-3" controlId="postalCode">
                    <Form.Label>Postal Code</Form.Label>
                    <Form.Control type="text" placeholder="Enter Postal Code" required value={postalCode} onChange={e => setPostalCode(e.target.value)}></Form.Control>
                </Form.Group>

                <Form.Group className="my-3" controlId="country">
                    <Form.Label>Country</Form.Label>
                    <Form.Control type="text" placeholder="Enter Country" required value={country} onChange={e => setCountry(e.target.value)}></Form.Control>
                </Form.Group>
                <Button type="submit" variant="primary">Continue</Button>
            </Form>
        </FormContainer>
    )
}

export default ShippingScreen;
